>v1.0.0 : Release (11.08.2018)
>>The app is functional.

>v1.0.1 : (11.08.2018)
>>Creation of this 'Changelog.txt' file.
>>The name of the database file was changed. This was motivated by a need to store multiple
  database files in a single location without name collision. A unique identifier was added
  at the end of the name.
>>Slight code cleanup in 'Send_database.java'. The email subject was modified, as the 'user ID'
  is redundant and can be changed.

>v1.0.2 (SP1) : (21.10.2018)
>>The 'Show Fuel Duration' bug was investigated, but the feature seems to work
>>The deprecated build.gradle commands that were triggering warnings at compile-time were
  replaced by their newer counterparts.

>v1.0.3 (SP1) : (01.11.2018)
>>Minor tutorial text changes
>>Tutorial: If there is no 'out of fuel penalty', then losing points due to
  not refueling isn't mentioned.

>v1.0.4 (SP1) : (05.11.2018)
>>Tutoriel: Added a 'go back one page' button, resolved a few bugs and slight code
  optimisation.

>v1.0.5 (SP1) : (10.11.2018)
>>A lot of code cleanup (C code mostly).
>>Some more getting accustomed to GitLab's environment (.gitignore and such).
>>Applied change: now the user-entered code can be as low as '1' (previously '1000'), and
  age/id-code bounds have been moved to 'res/values/integer.xml'.
>>Added a 'Todo' file, to keep track of what's left to do.

>v1.0.6 (SP1) : (25.11.2018)
>>Some code cleanup, mostly aesthetic to make the header files more readable.
>>New and improved way of displaying 'image_fx' on the screen
>>>New 'image_fx' as proof-of-concept: out-of-fuel
>>Now, when there is a point loss/gain, its value will scroll up (near the score box) in a visually
  pleasant way.
>>User interaction with the pause button is now logged as well.

>v1.0.7 (SP1) : (26.11.2018)
>>Applied changes to tutorial background 2,3,4 and 6; Making the fingerprint
  more visible (bigger and slightly highlighted. Also, the previously missing 'score box' is now
  present in the tutorial background.
>>Changed 'Tutorial.java' accordingly.
>>Reorganised 'Todo.txt'.
>>Applied changes to the game configuration : added 'Need to refuel' and 'Display Pause button'
  These are switches to modify key aspects of the game, along with the already-present
   'Display numeric clock'.
>>>Note: these may still be buggy
>>Added a 'release' folder where we'll put compiled ans signes APKs of each version.

>v1.0.8 (SP1) : (26.11.2018)
>>Tutorial: It's now aware of the 'Need to refuel' and 'Display Pause button' configuration options
  and will skip the concerned pages when appropriate. Also the 'go back' option is removed from the
  last page for technical and design-related concerns.
>>Bugfix: the tutorial will now create a 'cruiser_config.csv' file when it doesn't exist, avoiding
  a 'something went wrong'-type fo message to show up the first time the game is launched or after
  the file was deleted.

>v1.0.9 (SP1) : (02.12.2018)
>>Game: A 'pre-race countdown' was implemented, and some placeholder resources along with it.
>>Game logic bugfix: The 'time_left' variable was being decremented by chunks of 1000+ε [ms] and
  this caused second-skipping issues (it didn't affect the duration of the game, only the digital
  clock skipped seconds). The logic was changed to correct this behaviour.

>v1.0.10 (SP1) : (17.01.2019)
>>Git: Fused uncommited changes to 'master'
>>Attila: fixed refuel animation, reduced redundancy, better asset 'fuel_up.png'
>>Bugfix Tutorial: Optional pages should work with game configuration now

>v1.0.11 (SP1) : (14.03.2019)
>>Git: Fused uncommited changes to 'master' and fixed branches errors
>>Attila: better asset 'countdown*'
>>Updated manual

>v1.0.12 (SP1) : (08.04.2019)
>>Add sound assets ("out of fuel" sound_fx, "refueling" sound_fx, "game over" sound_fx)
 and code them into the game.
>>Disable short toast in tutorial.
>>Game logic bugfix : We spent many hours trying to correct a bug that came across when introducing
  more sound_fx. The game would deadlock before displaying countdown. Long story short,
  an important time value was incorrectly initiated.

>v1.0.13 (SP1) : (14.04.2019)
>>Bugfix: Malformed 'stat_db_file_1stline' in 'strings.xml' caused misalignment in output database file
>>Manual: multiple typo correction and info update

>v1.0.14 (SP1) : (21.05.2019)
>>Cruiser: Add two switches for audiovisual feedback in cruiser config (disable outOfFuel and/or all).
>>>Optimisation: When audio feedback is deactivated, the correspondant audio file isn't loaded or freed.
>>Cruiser: Fixed the way transparency was handled regarding player's car (in collision events).
>>>This fixes the player's car being transparent during countdown !
>>Cruiser: Some code cleanup, commenting and a few grobal constants declared.
>>Manual: Converted LibreOffice document to LaTeX project, added backup contact, minor fixes.

