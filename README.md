# outcar

OutCar is a game developed for University of Geneva, for psychology studies. The game consists of avoiding traffic and refueling periodically (just by pressing a button).

This game was under active development until May 2019. The last version was v1.0.14.

**SP1** stands for *Service Pack 1*, for this is the first major update since the initial
release.


## Notes

* 'outcar' is the internal/developper name for this app. The user-visible name is 'Cal 
Cruiser'.
* You can find a compiled and signed APK in the 'release' folder
* This project comes with a manual. You can request a copy via email at David.Rodriguez.1@etu.unige.ch

## License

OutCar is available under the GNU GPLv3 license. Here is the disclaimer :
"OutCar is a game developed for University of Geneva, for psychology studies. 
The game consists of avoiding traffic and refueling periodically.
Copyright (C) 2018-2019 David Rodriguez, Attila Nacsa, Mark Tropin

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program (see LICENSE).

Contact the authors : David.Rodriguez.1@etu.unige.ch
                      Attila.Nacsa@etu.unige.ch

Please note that SDL2 is used and can be found within this project. Its
code wasn't altered and we don't claim any of it."
